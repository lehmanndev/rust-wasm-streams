# Testing streams with Rust WASM

## Prerequisites

- rust and cargo
- libstd-rust-dev-wasm32
- wasm-pack (crate)


## Build

```
wasm_streams $ make
         web $ make
```


## Look at it

After building run

```
web $ make server
```

Then visit http://localhost:8080/
