# Notes

## Wasm pack install error

### Problem

```
$ cargo install wasm-pack --version 0.10.3
  ...
Caused by:
  package `anstyle-query v1.0.0` cannot be built because it requires rustc 1.64.0 or newer, while the currently active rustc version is 1.63.0
  Try re-running cargo install with `--locked`
```

### Solution

Using `--locked` without a certain version was not sufficient, the previous version (0.10.3 around April 2023) was added. 

```
$ cargo install wasm-pack --version 0.10.3 --locked
```
