use wasm_bindgen::prelude::*;
use web_sys::ReadableStream;
use web_sys::ReadableStreamDefaultReader;
use serde_wasm_bindgen;
use serde;
use wasm_bindgen_futures::JsFuture;

pub mod console;

#[wasm_bindgen]
extern {
    pub fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet(name: &str) {
    console::console_log(&format!("Hello from Rust {}!", name));
    alert(&format!("Hello, {}!", name));
}

#[derive(serde::Serialize, serde::Deserialize)]
struct ReadableStreamDefaultReadResult<T> {
    pub value: T,
    pub done: bool,
}

#[wasm_bindgen]
pub async fn process(stream: ReadableStream) {
    let reader = ReadableStreamDefaultReader::new(&stream);

    if let Err(err) = reader {
        console::console_log(&format!("Error getting reader {:?}", err));
        return;
    }

    let reader = reader.unwrap();
    let mut s = String::new();

    loop {
        let reader_promise = JsFuture::from(reader.read());
        let result = reader_promise.await;

        if let Err(err) = result {
            console::console_log(&format!("Error reading from reader {:?}", err));
            break;
        }

        let result: ReadableStreamDefaultReadResult<Option<Vec<u8>>> =
        serde_wasm_bindgen::from_value(result.unwrap()).unwrap();

        if let Some(buffer) = result.value {
            console::console_log(&format!("chunk {} {:?}", type_of(&buffer).as_str(), buffer));
            s.push_str(&String::from_utf8(buffer).unwrap());
        }

        if result.done {
            console::console_log(&format!("stream ended"));
            break;
        }
    }

    console::console_log(&format!("Loaded '{}'", s));
}

fn type_of<T>(_: &T) -> String {
    format!("{}", std::any::type_name::<T>())
}
